public class Machine {
    int water, coffeeBeans, milk, coffeeCups;
    int waterRequired, coffeeRequired, milkRequired;

    public Machine(int water, int coffeeBeans, int milk, int coffeeCups) {
        this.water = water;
        this.coffeeBeans = coffeeBeans;
        this.milk = milk;
        this.coffeeCups = coffeeCups;
    }

    protected void initializeSpecificCoffee(){
        waterRequired = 0;
        coffeeRequired = 0;
        milkRequired = 0;
    }

    protected int prepareCoffee(){
        int tempWater = water/waterRequired;
        int tempCoffee = coffeeBeans/coffeeRequired;
        int tempMilk = milk/milkRequired;
        int tempMinCoffeeCups = Math.min(tempWater, Math.min(tempCoffee, tempMilk));
        if (tempMinCoffeeCups >= coffeeCups){
            System.out.print("Yes, I can make that amount of coffee ");
            if (tempMinCoffeeCups - coffeeCups > 0){
                System.out.print("(I can make " + (tempMinCoffeeCups - coffeeCups) + " more coffee)" );
            }
        }else {
            System.out.print("No, I cannot make that amount of coffee ");
        }
        return 0;
    }
}
