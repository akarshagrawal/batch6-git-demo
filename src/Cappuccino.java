//200 ml of water, 100 ml of milk, and 12 g of coffee
public class Cappuccino extends Machine {
    int cappuccinoCups;
    public Cappuccino(int water, int coffeeBeans, int milk, int coffeeCups) {
        super(water, coffeeBeans, milk, coffeeCups);
        cappuccinoCups = coffeeCups;
    }

    @Override
    protected int prepareCoffee() {
        return super.prepareCoffee();
    }

    @Override
    protected void initializeSpecificCoffee() {
        waterRequired = 200;
        milkRequired = 100;
        coffeeRequired = 12;
    }
}
