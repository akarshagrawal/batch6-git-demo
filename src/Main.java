import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int water, coffeeBeans, milk, coffeeCups;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Write how many ml of water the coffee machine has: ");
        water = scanner.nextInt();
        System.out.print("Write how many ml of milk the coffee machine has: ");
        milk = scanner.nextInt();
        System.out.print("Write how many ml of coffee the coffee machine has: ");
        coffeeBeans = scanner.nextInt();
        System.out.print("Write how many cups of coffee the coffee machine has: ");
        coffeeCups = scanner.nextInt();

        Cappuccino cappuccino = new Cappuccino(water,milk,coffeeBeans,coffeeCups);
        cappuccino.initializeSpecificCoffee();
        cappuccino.prepareCoffee();

    }


}
